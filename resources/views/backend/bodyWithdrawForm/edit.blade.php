@extends('backend.layouts.app')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                शव झिकाउनको लागि निवेदन

            </h1>
            <ol class="breadcrumb">
                <li><a href="#"> Dashboard</a></li>
                <li><a href="#">शव झिकाउनको लागि निवेदन
                    </a></li>
                <li class="active">सम्पादन गर्नुहोस्</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            @include('backend.message.flash')
            <div class="row">
                <div class="col-md-12" id="listing">
                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title">शव झिकाउनको लागि निवेदन
                            </h3>

                            <a href="{{url('bodyWithdrawForm/create')}}" class="pull-right boxTopButton" id="add"
                               data-toggle="tooltip" title="Add New"><i class="fa fa-plus-circle fa-2x"></i></a>

                            <a href="{{url('/bodyWithdrawForm')}}" class="pull-right boxTopButton" data-toggle="tooltip"
                               title="View All"><i class="fa fa-list fa-2x"></i></a>

                            <a href="{{URL::previous()}}" class="pull-right boxTopButton" data-toggle="tooltip"
                               title="Go Back">
                                <i class="fa fa-arrow-circle-left fa-2x"></i></a>
                        </div>
                        <div class="box-body">
                            {!! Form::model($edits, ['method'=>'put','route'=>['bodyWithdrawForm.update',$edits->id],'enctype'=>'multipart/form-data','file'=>true]) !!}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group {{ ($errors->has('district_id'))?'has-error':'' }}">
                                            <label>जिल्ला</label>
                                            {{Form::select('district_id',$districts->pluck('nepali_name','id'),Request::get('district_id'),['class'=>'form-control select2','id'=>'district_id','placeholder'=>
                                            'वैदेशिक रोजगारमा गएको व्यक्तिको जिल्ला छनौट गर्नुहोस्'])}}
                                            {!! $errors->first('district_id', '<span class="text-danger">:message</span>') !!}
                                        </div>

                                        <div class="form-group {{ ($errors->has('muni_id'))?'has-error':'' }}">
                                            <label>नगरपालिका</label>
                                            {{Form::select('muni_id',$muni->pluck('muni_name','id'),Request::get('muni_id'),['class'=>'form-control select2','id'=>'group_id','placeholder'=>
                                            'वैदेशिक रोजगारमा गएको व्यक्तिको नगरपालिका छनौट गर्नुहोस्'])}}
                                            {!! $errors->first('muni_id', '<span class="text-danger">:message</span>') !!}
                                        </div>
                                        <div class="form-group {{ ($errors->has('country_id'))?'has-error':'' }}">
                                            <label>गएको देश</label>
                                            {{Form::select('country_id',$country->pluck('country_name','id'),Request::get('country_id'),['class'=>'form-control select2','id'=>'group_id','placeholder'=>
                                            'वैदेशिक रोजगारमा गएको देश छनौट गर्नुहोस्'])}}
                                            {!! $errors->first('country_id', '<span class="text-danger">:message</span>') !!}
                                        </div>

                                        <div class="form-group {{ ($errors->has('relation'))?'has-error':'' }}">
                                            <label>सम्बन्ध</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                {!! Form::text('relation',null,['class'=>'form-control','placeholder'=>'मृत व्यक्ति संग आवेदकको सम्बन्ध']) !!}
                                            </div>
                                            {!! $errors->first('relation', '<span class="text-danger">:message</span>') !!}
                                        </div>
                                        <div class="form-group {{ ($errors->has('dp_name'))?'has-error':'' }}">
                                            <label>नाम</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                {!! Form::text('dp_name',null,['class'=>'form-control','placeholder'=>'मृत व्यक्तिको नाम']) !!}
                                            </div>
                                            {!! $errors->first('dp_name', '<span class="text-danger">:message</span>') !!}
                                        </div>
                                        <div class="form-group {{ ($errors->has('passport_no'))?'has-error':'' }}">
                                            <label>पासपोर्ट नम्बर</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                {!! Form::text('passport_no',null,['class'=>'form-control','placeholder'=>'मृत व्यक्तिको पासपोर्ट नम्बर']) !!}
                                            </div>
                                            {!! $errors->first('passport_no', '<span class="text-danger">:message</span>') !!}
                                        </div>
                                        <div class="form-group {{ ($errors->has('work_place_name'))?'has-error':'' }}">
                                            <label>कार्य स्थान नाम</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                {!! Form::text('work_place_name',null,['class'=>'form-control','placeholder'=>'मृत व्यक्तिको कार्य स्थान नाम']) !!}
                                            </div>
                                            {!! $errors->first('work_place_name', '<span class="text-danger">:message</span>') !!}
                                        </div>

                                        <div class="form-group {{ ($errors->has('wp_type'))?'has-error':'' }}">
                                            <label>कार्य स्थान प्रका
                                            </label>
                                            {!! Form::select('wp_type',$wpTypes,null,['id' => 'wpType','style' => 'width:100%','class'=>'form-control select2','placeholder'=>'कार्य स्थान प्रकार']) !!}
                                            {!! $errors->first('wp_type', '<span class="label label-danger">:message</span>') !!}
                                        </div>

                                        <div class="form-group {{ ($errors->has('wp_tel_no'))?'has-error':'' }}">
                                            <label>फोन नम्बरर</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                {!! Form::number('wp_tel_no',null,['class'=>'form-control','placeholder'=>'कार्य स्थान फोन नम्बर']) !!}
                                            </div>
                                            {!! $errors->first('wp_tel_no', '<span class="text-danger">:message</span>') !!}
                                        </div>
                                        <div class="form-group {{ ($errors->has('death_date'))?'has-error':'' }}">
                                            <label>मृत्यु मिति</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                {!! Form::date('death_date',null,['class'=>'form-control','placeholder'=>'मृत्यु मिति']) !!}
                                            </div>
                                            {!! $errors->first('death_date', '<span class="text-danger">:message</span>') !!}
                                        </div>
                                        <div class="form-group {{ ($errors->has('applicant_name'))?'has-error':'' }}">
                                            <label>आवेदकको नाम</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                {!! Form::text('applicant_name',null,['class'=>'form-control','placeholder'=>'आवेदकको पूरा नाम']) !!}
                                            </div>
                                            {!! $errors->first('applicant_name', '<span class="text-danger">:message</span>') !!}
                                        </div>
                                        <div class="form-group {{ ($errors->has('app_relation'))?'has-error':'' }}">
                                            <label>आवेदकको सम्बन्धम</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                {!! Form::text('app_relation',null,['class'=>'form-control','placeholder'=>'मृत व्यक्तिसँग आवेदकको सम्बन्ध']) !!}
                                            </div>
                                            {!! $errors->first('app_relation', '<span class="text-danger">:message</span>') !!}
                                        </div>
                                        <div class="form-group {{ ($errors->has('app_address'))?'has-error':'' }}">
                                            <label>आवेदकको ठेगाना</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                {!! Form::text('app_address',null,['class'=>'form-control','placeholder'=>'आवेदकको ठेगाना']) !!}
                                            </div>
                                            {!! $errors->first('app_address', '<span class="text-danger">:message</span>') !!}
                                        </div>
                                        <div class="form-group {{ ($errors->has('app_tel_no'))?'has-error':'' }}">
                                            <label>आवेदकको फोन नम्बर</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                {!! Form::number('app_tel_no',null,['class'=>'form-control','placeholder'=>'आवेदकको फोन नम्बर']) !!}
                                            </div>
                                            {!! $errors->first('app_tel_no', '<span class="text-danger">:message</span>') !!}
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">
                                                {{trans('app.save')}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection