@extends('backend.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                शव झिकाउनको लागि निवेदन
            </h1>
            <ol class="breadcrumb">
                <li><a style="color: gray;" href="{{url('/dashboard')}}"><i
                                class="fa fa-dashboard"></i> {{trans('app.dashboard')}}</a></li>
                <li><a href="#">शव झिकाउनको लागि निवेदन
                    </a></li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            @include('backend.message.flash')
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title"><strong>निवेदन सूचीहरू
                        </strong></h3>
                    <a href="{{url('/bodyWithdrawForm/create')}}" class="pull-right boxTopButton" id="add" data-toggle="tooltip"
                       title="Add New"><i class="fa fa-plus-circle fa-2x"></i></a>

                    <a href="{{url('/bodyWithdrawForm')}}" class="pull-right boxTopButton" data-toggle="tooltip"
                       title="View All"><i class="fa fa-list fa-2x"></i></a>

                    <a href="{{URL::previous()}}" class="pull-right boxTopButton" data-toggle="tooltip" title="Go Back">
                        <i class="fa fa-arrow-circle-left fa-2x"></i></a>
                </div>
                <div class="box-body">
                    <div class="table-responsive">

                        <table id="example1" class="table table-bordered table-hover table-responsive">
                            <thead>
                            @if(!empty($bodyWithdrawForms))
                                <tr>
                                <th style="width: 10px">क्रम संख्या</th>
                                <th>आवेदन मिति</th>
                                <th>पासपोर्ट नम्बर</th>
                                <th>मृत व्यक्तिको नाम</th>
                                <th>आवेदकको नाम</th>
                                <th>मृत व्यक्तिसँग आवेदकको सम्बन्ध</th>
                                <th style="width: 50px" class="text-right">कार्य</th>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse($bodyWithdrawForms as $key=>$bodyWithdrawForm)
                                <tr>
                                    <th scope=row>{{++$key}}</th>
                                    <td>{{$bodyWithdrawForm->date}}</td>
                                    <td>{{$bodyWithdrawForm->passport_no}}</td>
                                    <td>{{$bodyWithdrawForm->dp_name}}</td>
                                    <td>{{$bodyWithdrawForm->applicant_name}}</td>
                                    <td>{{$bodyWithdrawForm->app_relation}}</td>
                                    <td class="text-right">
                                        <a href="{{url('bodyWithdrawForm/'.$bodyWithdrawForm->id .'/edit')}}"
                                           class="text-info btn btn-xs btn-default">
                                            <i class="fa fa-pencil-square-o"></i>
                                        </a>
                                        <a href="{{url('bodyWithdrawForm/'.$bodyWithdrawForm->id)}}"
                                           class="text-info btn btn-xs btn-default">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </td>
                                </tr>
                            @empty
                            @endforelse
                            @else
                                <div class="col-md-12">
                                    <label class="form-control label-danger">No records found</label>
                                </div>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection