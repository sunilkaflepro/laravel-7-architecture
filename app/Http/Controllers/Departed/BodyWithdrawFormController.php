<?php

namespace App\Http\Controllers\Departed;

use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController;
use App\Repository\Configurations\DistrictRepository;
use App\Repository\Configurations\MunicipalityRepository;
use App\Repository\Configurations\CountryRepository;
use App\Repository\Departed\BodyWithdrawFormRepository;
use Illuminate\Http\Request;
use App\BodyWithdrawForm;
use Illuminate\Support\Carbon;
use Symfony\Component\Console\Input\Input;


class BodyWithdrawFormController extends BaseController
{
    /**
     * @var DistrictRepository
     */
    private $districtRepository;
    /**
     * @var MunicipalityRepository
     */
    private $municipalityRepository;
    /**
     * @var CountryRepository
     */
    private $countryRepository;
    /**
     * @var BodyWithdrawForm
     */
    private $bodyWithdrawForm;
    /**
     * @var BodyWithdrawFormRepository
     */
    private $bodyWithdrawFormRepository;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(DistrictRepository $districtRepository, MunicipalityRepository $municipalityRepository,CountryRepository $countryRepository,
                                BodyWithdrawForm $bodyWithdrawForm, BodyWithdrawFormRepository $bodyWithdrawFormRepository)
    {
        parent::__construct();


        $this->districtRepository = $districtRepository;
        $this->municipalityRepository = $municipalityRepository;
        $this->countryRepository = $countryRepository;
        $this->bodyWithdrawForm = $bodyWithdrawForm;
        $this->bodyWithdrawFormRepository = $bodyWithdrawFormRepository;
    }
    public function index()
    {
        $bodyWithdrawForms = $this->bodyWithdrawFormRepository->all();
        return view('backend.bodyWithdrawForm.index', compact('bodyWithdrawForms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
        $wpTypes = ['company'=>'Company','house'=>'House','illegal'=> 'illegal'];
        $country=$this->countryRepository->all();
        $districts=$this->districtRepository->all();
        $muni=$this->municipalityRepository->all();
        return view('backend.bodyWithdrawForm.add', compact('wpTypes','districts','muni','country'));
        } catch (\Exception $e) {
            $e = $e->getMessage();
            session()->flash('error', 'Exception : ' . $e);
            return back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $value = $request->all();
            $value['date'] = Carbon::now();
            $data = $this->bodyWithdrawForm->create($value);
            if ($data) {
                session()->flash('success', 'Successfully Created!');
                return back();
            } else {
                session()->flash('success', 'Could not be Created!');
                return back()->withInput(Input::all());
            }
        } catch (\Exception $e) {
            $e = $e->getMessage();
            session()->flash('error', 'Exception : ' . $e);
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = (int)$id;
            $wpTypes = ['company'=>'Company','house'=>'House','illegal'=> 'illegal'];
            $country=$this->countryRepository->all();
            $districts=$this->districtRepository->all();
            $muni=$this->municipalityRepository->all();
            $edits = $this->bodyWithdrawFormRepository->findById($id);
            if (!empty($edits)) {
                return view('backend.bodyWithdrawForm.edit', compact('edits','wpTypes','districts','muni','country'));

            } else {
                session()->flash('error', 'Id could not be obtained!');
                return back();
            }

        } catch (\Exception $e) {
            $exception = $e->getMessage();
            session()->flash('error', 'EXCEPTION :' . $exception);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = (int)$id;
        try {
            $bodyWithdrawForms = $this->bodyWithdrawFormRepository->findById($id);
            $value = $request->all();
            if ($bodyWithdrawForms) {
                $bodyWithdrawForms->fill($value)->save();
                session()->flash('success', 'Updated successfully!');
                return redirect(route('bodyWithdrawForm.index'));
            } else {

                session()->flash('error', 'No record with given id!');

                return back();
            }
        } catch (\Exception $e) {
            $exception = $e->getMessage();
            session()->flash('error', 'EXCEPTION:' . $exception);

            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
