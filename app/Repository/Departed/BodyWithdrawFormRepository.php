<?php
/**
 * Created by PhpStorm.
 * User: ym
 * Date: 3/26/18
 * Time: 11:29 AM
 */

namespace App\Repository\Departed;


use App\BodyWithdrawForm;

class BodyWithdrawFormRepository
{

    /**
     * @var BodyWithdrawForm
     */
    private $bodyWithdrawForm;

    public function __construct(BodyWithdrawForm $bodyWithdrawForm)
    {

        $this->bodyWithdrawForm = $bodyWithdrawForm;
    }

    public function all()
    {
        $result = $this->bodyWithdrawForm->all();
        return $result;
    }
    public function findById($id)
    {
        $result = $this->bodyWithdrawForm->find($id);
        return $result;
    }
}