<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BodyWithdrawForm extends Model
{
    protected $dates = ['deleted_at'];
    use SoftDeletes;
    
    protected $fillable = [
        'date','district_id', 'muni_id', 'country_id', 'relation',
        'dp_name', 'passport_no','work_place_name','wp_type','wp_tel_no','death_date','applicant_name','app_relation',
        'app_address','app_tel_no'
    ];

    public function district(){
        return $this->belongsTo('App\Models\Configurations\District','district_id','id');
    }
    public function municipality(){
        return $this->belongsTo('App\Models\Configurations\Municipality','muni_id','id');
    }
    public function country(){
        return $this->belongsTo('App\Models\Configurations\Country','country_id','id');
    }
}
