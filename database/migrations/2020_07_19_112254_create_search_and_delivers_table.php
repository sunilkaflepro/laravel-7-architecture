<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSearchAndDeliversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('search_and_delivers', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->integer('district_id')->unsigned();
            $table->foreign('district_id')->references('id')->on('districts');
            $table->integer('muni_id')->unsigned();
            $table->foreign('muni_id')->references('id')->on('municipalities');
            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('countries');
            $table->string('relation');
            $table->string('dp_name');  //deadPerson
            $table->string('passport_no');
            $table->string('work_place_name');
            $table->string('wp_type');
            $table->string('wp_tel_no');
            $table->date('death_date');
            $table->string('applicant_name');
            $table->string('app_relation');
            $table->string('app_address');
            $table->string('app_tel_no');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('search_and_delivers');
    }
}
